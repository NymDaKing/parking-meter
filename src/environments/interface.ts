export interface Environment {
    production: boolean;
    api: string;
    url: string;
}
