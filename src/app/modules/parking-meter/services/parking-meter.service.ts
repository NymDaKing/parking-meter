import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParkingListItem, ParkingDetailItem } from '../parking-meter.model';
import { map, Observable, Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ParkingMeterService {
    constructor(public http: HttpClient) {}

    getParkingMeters(): Observable<ParkingListItem[]> {
        return this.http.get(`${environment.url}/items.json`).pipe(
            map((response: { [id: string]: any }) => {
                return Object.keys(response).map((key) => ({
                    id: key,
                    ...response[key],
                }));
            }),
        );
    }

    getParkingMeter(parkingMeterId: string) {
        return this.http.get<ParkingDetailItem>(`${environment.url}/items/${parkingMeterId}.json`);
    }

    createParkingMeter(item: Omit<ParkingDetailItem, 'id'>): Observable<ParkingDetailItem> {
        return this.http.post<ParkingDetailItem>(`${environment.url}/items.json`, item);
    }

    updateParkingMeter(parkingMeterId: string, item: Partial<ParkingDetailItem>): Observable<ParkingDetailItem> {
        return this.http.patch<ParkingDetailItem>(`${environment.url}/items/${parkingMeterId}.json`, item);
    }
}
