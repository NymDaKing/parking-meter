import { Component } from '@angular/core';

@Component({
    templateUrl: 'parking-meter.component.html',
    styleUrls: ['parking-meter.component.scss'],
})
export class ParkingMeterComponent {}
