import { NgModule } from '@angular/core';
import { ParkingMeterDetailsComponent } from './components/parking-meter-details/parking-meter-details.component';
import { ParkingMeterListComponent } from './components/parking-meter-list/parking-meter-list.component';
import { RouterModule } from '@angular/router';
import { ParkingMeterComponent } from './parking-meter.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { ParkingMeterCreateDialogComponent } from './components/parking-meter-create-dialog/parking-meter-create-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
    declarations: [
        ParkingMeterComponent,
        ParkingMeterDetailsComponent,
        ParkingMeterListComponent,
        ParkingMeterCreateDialogComponent,
    ],
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: ParkingMeterComponent,
                children: [
                    { path: '', component: ParkingMeterListComponent },
                    { path: ':id', component: ParkingMeterDetailsComponent },
                ],
            },
        ]),
        CommonModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatGridListModule,
        MatCardModule,
        MatDialogModule,
        MatDividerModule,
        MatFormFieldModule,
        MatInputModule,
        MatSlideToggleModule,
    ],
})
export class ParkingMeterModule {}
