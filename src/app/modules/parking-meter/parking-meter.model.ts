export interface ParkingListItem {
    id: string;
    address: string;
}

export interface ParkingDetailItem extends ParkingListItem {
    status: 'Disabled' | 'Enabled';
    usages: number;
}
