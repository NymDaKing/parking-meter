import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ParkingMeterService } from '../../services/parking-meter.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'parking-meter-create-dialog',
    templateUrl: 'parking-meter-create-dialog.component.html',
})
export class ParkingMeterCreateDialogComponent {
    address: FormControl<string | null> = this.fb.control(null, Validators.required);

    constructor(
        private fb: FormBuilder,
        private parkingService: ParkingMeterService,
        private dialogRef: MatDialogRef<ParkingMeterCreateDialogComponent>,
    ) {}

    create() {
        if (this.address.invalid || !this.address.value) {
            return;
        }
        this.parkingService
            .createParkingMeter({
                address: this.address.value,
                status: 'Enabled',
                usages: 0,
            })
            .subscribe(() => this.closeDialog(true));
    }

    closeDialog(result?: boolean): void {
        this.dialogRef.close(result);
    }
}
