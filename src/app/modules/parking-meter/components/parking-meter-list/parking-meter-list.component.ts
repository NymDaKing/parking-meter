import { Component } from '@angular/core';
import { ParkingListItem } from '../../parking-meter.model';
import { ParkingMeterCreateDialogComponent } from '../parking-meter-create-dialog/parking-meter-create-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ParkingMeterService } from '../../services/parking-meter.service';

@Component({
    templateUrl: './parking-meter-list.component.html',
    styleUrls: ['./parking-meter-list.component.scss'],
})
export class ParkingMeterListComponent {
    parkingList: ParkingListItem[] = [];

    constructor(private dialog: MatDialog, private parkingService: ParkingMeterService) {
        this.getParkingMeters();
    }

    getParkingMeters() {
        this.parkingService.getParkingMeters().subscribe((parkingList) => {
            this.parkingList = parkingList.sort((a, b) => (a.address > b.address ? 1 : b.address > a.address ? -1 : 0));
        });
    }

    openCreateDialog(): void {
        const dialogRef = this.dialog.open(ParkingMeterCreateDialogComponent, {
            width: '250px',
        });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.getParkingMeters();
            }
        });
    }
}
