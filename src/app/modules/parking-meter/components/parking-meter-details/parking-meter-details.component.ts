import { Component } from '@angular/core';
import { ParkingDetailItem } from '../../parking-meter.model';
import { ParkingMeterService } from '../../services/parking-meter.service';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs';

@Component({
    templateUrl: './parking-meter-details.component.html',
    styleUrls: ['./parking-meter-details.component.scss'],
})
export class ParkingMeterDetailsComponent {
    parkingMeter: ParkingDetailItem | undefined;

    constructor(private parkingService: ParkingMeterService, private route: ActivatedRoute) {
        this.route.params.pipe(take(1)).subscribe((params) => this.getParkingMeter(params['id']));
    }

    getParkingMeter(parkingMeterId: string) {
        this.parkingService.getParkingMeter(parkingMeterId).subscribe((parkingMeter) => {
            this.parkingMeter = { ...parkingMeter, id: parkingMeterId };
        });
    }

    update(item: Partial<ParkingDetailItem>) {
        if (this.parkingMeter) {
            const parkingMeterId = this.parkingMeter.id;
            this.parkingService
                .updateParkingMeter(parkingMeterId, { ...item })
                .subscribe(() => this.getParkingMeter(parkingMeterId));
        }
    }
}
